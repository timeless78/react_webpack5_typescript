# react_webpack5_typescript



## Getting started

react18 + webpack5 + typescript
리액트18 + 웹팩5 + 타입스크립트를 이용한 스타터킷.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/timeless78/react_webpack5_typescript.git
git branch -M main
git push -uf origin main
```

## 최초 다운로드 후 해야 할 것

```
npm install -g yarn (yarn 설치)
yarn install(node_modules 다운로드)
yarn dev(실행 (개발용)-config/webpack.dev.js 기반)
yarn build(빌드 (운영용)-config/webpack.prod.js 기반)
```

## 추천하는 설치할 만한 것들

- [ ] styled-components / Emotion / TailWind (스타일링)
- [ ] redux / recoil(추천) / mobx / Jotai / Zustand ( Client 상태관리)
- [ ] react Query (Server 상태관리)
- [ ] axios(API 통신)
- [ ] lodash-es(자바스크립트 라이브러리(객체,배열,구조 변환 등))
- [ ] dayjs(날짜)
- [ ] immer(불변성)
- [ ] jest & @Testing-library/react (테스트)


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# 아래의 글에서 참고하여 작성 되었습니다.

떠나시기전에 깃허브 star와 블로그의 https://ryuhojin.tistory.com/19 글의 하트 한번만 눌러주세요.(유일한 재미)
